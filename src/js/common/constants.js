'use strict';

(function() {
  angular
    .module('mcfadyen')
    .constant('appConstants', {
      TEST: 'Ok',
      API_URL: 'https://mcfadyen.herokuapp.com/api'
    });
})();
