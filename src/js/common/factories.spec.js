describe('Common factories', function() {
  var Constants;

  describe('Constants', function() {
    beforeEach(function() {
      module('mcfadyen');
      inject(function($injector) {
        Constants = $injector.get('Constants');
      });
    });

    it('should exists', function() {
      expect(Constants).toBeDefined();
    });

    describe('get()', function() {
      it('should exists', function() {
        expect(Constants.get).toBeDefined();
      });

      it('should return a constant', function() {
        expect(Constants.get('TEST')).toEqual('Ok');
      });
    });
  });
});
