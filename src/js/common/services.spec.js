'use strict';

describe('Common services', function() {
  var dataService;
  var $httpBackend;
  var $sessionStorage;
  var urlSuccess = 'http://localhost/mcfayden';
  var urlError = 'http://localhost/mcfayden/error';
  var jsonRes = {id: 1, name: 'Product 01', price: 59.90, token: '12345'};

  describe('dataService', function() {
    beforeEach(function() {
      module('templates');
      module('ngSessionStorage');
      module('mcfadyen');

      inject(function($injector) {
        dataService = $injector.get('dataService');
        $httpBackend = $injector.get('$httpBackend');
        $sessionStorage = $injector.get('$sessionStorage');

        $httpBackend.when('GET', urlSuccess).respond(200, jsonRes);
        $httpBackend.when('GET', urlError).respond(404);
      });
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should exists', function() {
      expect(dataService).toBeDefined();
    });

    describe('getData()', function() {
      it('should exists', function() {
        expect(dataService.getData).toBeDefined();
      });

      it('should return an object when passing data', function() {
        var config = {method: 'GET', url: urlSuccess, data: {id: 1}};

        dataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an object when passing no data', function() {
        var config = {method: 'GET', url: urlSuccess};

        dataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should add x-access-token to header', function() {
        var config = {method: 'GET', url: urlSuccess};

        $sessionStorage.put('api_token', 'mcfadyen');

        dataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should not add x-access-token to header', function() {
        var config = {method: 'GET', url: urlSuccess};

        $sessionStorage.remove('api_token', 'mcfadyen');

        dataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an error from promise', function() {
        var config = {method: 'GET', url: urlError};

        dataService
          .getData(config)
          .catch(function(error) {
            expect(error).toBe('Error 404');
          });

        $httpBackend.flush();
      });
    });
  });
});
