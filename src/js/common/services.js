'use strict';

(function() {
  angular
    .module('mcfadyen')
    .service('dataService', dataService);

  dataService.$inject = ['$q', '$http', '$sessionStorage'];

  /**
   * Data service
   */
  function dataService($q, $http, $sessionStorage) {
    return {
      getData: getData
    };

    /**
     * Make HTTP requests
     */
    function getData(config) {
      var request;
      var data = !config.data ? {} : config.data;
      var header = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };

      if ($sessionStorage.get('api_token')) {
        header['x-cart-token'] = $sessionStorage.get('api_token');
      }

      request = $http({
        cache: false,
        method: config.method,
        data: $.param(data),
        url: config.url,
        headers: header,
        timeout: 10000
      });

      return request.then(handleSuccess, handleError);
    }

    /**
     * Handle success response from server
     */
    function handleSuccess(response) {
      return response.data;
    }

    /**
     * Handle failed response from server
     */
    function handleError(response) {
      return $q.reject('Error ' + response.status);
    }
  }
})();
