describe('MainController', function() {
  var MainController;
  var MainModel;
  var Constants;
  var $httpBackend;
  var urlAdd;
  var urlDelete;
  var jsonShoppingCart = {
    error: false,
    data: {id: 1, amount: 150, items: [{quantity: 1, amount: 150}]}
  };

  beforeEach(function() {
    module('mcfadyen');
    module('templates');

    inject(function($injector, $controller) {
      MainController = $controller('MainController');
      MainModel = $injector.get('MainModel');
      Constants = $injector.get('Constants');
      urlAdd = Constants.get('API_URL') + '/shoppingcart/items';
      urlDelete = Constants.get('API_URL') + '/shoppingcart/items/12345';

      $httpBackend = $injector.get('$httpBackend');
      $httpBackend.when('POST', urlAdd).respond(200, jsonShoppingCart);
      $httpBackend.when('DELETE', urlDelete).respond(200, jsonShoppingCart);
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    spyOn(MainModel, 'getCart').and.callFake(function() {
      return {
        then: function(callback) {
          return callback({
            id: 1,
            amount: 150,
            items: [{
              id: 12345,
              quantity: 1,
              amount: 150
            }]
          });
        }
      };
    });

    spyOn(MainModel, 'getProducts').and.callFake(function() {
      return {
        then: function(callback) {
          return callback([{
            _id: 1,
            name: 'Product 1',
            price: 99.99
          }]);
        }
      };
    });
  });

  it('controller should exists', function() {
    expect(MainController).toBeDefined();
  });

  it('model should exists', function() {
    expect(MainModel).toBeDefined();
  });

  describe('init()', function() {
    beforeEach(function() {
      MainController.$onInit();
    });

    it('should exists', function() {
      expect(MainController.init).toBeDefined();
    });

    it('should call getProducts()', function() {
      expect(MainModel.getProducts).toHaveBeenCalled();
    });

    it('should call getCart()', function() {
      expect(MainModel.getCart).toHaveBeenCalled();
    });
  });

  describe('getProducts()', function() {
    it('should exists', function() {
      expect(MainController.getProducts).toBeDefined();
    });

    it('should load model', function() {
      expect(MainController.products).toBeUndefined();
      MainController.$onInit();
      expect(MainModel.getProducts).toHaveBeenCalled();
      expect(MainController.products).toBeDefined();
      expect(MainController.products.length).toBeGreaterThan(0);
      expect(MainController.products[0].name).toEqual('Product 1');
    });
  });

  describe('getCart()', function() {
    it('should exists', function() {
      expect(MainController.getCart).toBeDefined();
    });

    it('should load model', function() {
      expect(MainController.cart).toBeUndefined();
      MainController.$onInit();
      expect(MainModel.getCart).toHaveBeenCalled();
      expect(MainController.cart).toBeDefined();
      expect(MainController.cart.id).toBe(1);
      expect(MainController.cart.items.length).toBeGreaterThan(0);
    });
  });

  describe('addToCart()', function() {
    it('should exists', function() {
      expect(MainController.addToCart).toBeDefined();
    });

    it('should add a product to cart', function() {
      MainController.addToCart('12345');
      $httpBackend.flush();
    });
  });

  describe('should delete an item from cart', function() {
    it('should exists', function() {
      expect(MainController.deleteFromCart).toBeDefined();
    });

    it('should remove a product from cart', function() {
      MainController.deleteFromCart('12345');
      $httpBackend.flush();
    });
  });
});
