'use strict';

(function() {
  angular
    .module('mcfadyen.main')
    .controller('MainController', MainController);

  MainController.$inject = ['MainModel', 'dataService', 'Constants'];

  /**
   * Controller initialization
   */
  function MainController(MainModel, dataService, Constants) {
    var main = this;

    main.cartSession = undefined;
    main.products = undefined;
    main.cart = undefined;
    main.quantity = [];

    main.$onInit = init;
    main.getProducts = getProducts;
    main.getCart = getCart;
    main.addToCart = addToCart;
    main.deleteFromCart = deleteFromCart;
    main.init = init;

    /**
     * Initialization
     */
    function init() {
      getProducts();
      getCart();
    }

    /**
     * Get cart items from user current session
     */
    function getCart() {
      MainModel.getCart().then(function(data) {
        main.cart = data;
      });
    }

    /**
     * Get products list
     */
    function getProducts() {
      MainModel.getProducts().then(function(data) {
        main.products = data;
      });
    }

    /**
     * Add product to cart
     */
    function addToCart(productId) {
      dataService
        .getData({
          method: 'POST',
          url: Constants.get('API_URL') + '/shoppingcart/items',
          data: {
            productId: productId,
            quantity: main.quantity[productId]
          }
        })
        .then(function(data) {
          main.cart = data.data;
          main.quantity[productId] = 1;
        });
    }

    /**
     * Delete item from cart
     */
    function deleteFromCart(commerceItem) {
      dataService
        .getData({
          method: 'DELETE',
          url: Constants.get('API_URL') + '/shoppingcart/items/' + commerceItem
        })
        .then(function(data) {
          main.cart = data.data;
        });
    }
  }
})();
