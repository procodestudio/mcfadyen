'use strict';

(function() {
  angular
    .module('mcfadyen')
    .service('MainModel', MainModel);

  MainModel.$inject = ['$sessionStorage', 'Constants', 'dataService'];

  /**
   * Load model
   */
  function MainModel($sessionStorage, Constants, dataService) {
    var model = this;

    model.getProducts = getProducts;
    model.getCart = getCart;
    model.parseData = parseData;

    function getCart() {
      var data = {
        method: 'GET',
        url: Constants.get('API_URL') + '/shoppingcart'
      };

      return dataService
        .getData(data)
        .then(parseData);
    }

    /**
     * Get a list of all products
     */
    function getProducts() {
      var data = {
        method: 'GET',
        url: Constants.get('API_URL') + '/products'
      };

      return dataService
        .getData(data)
        .then(parseData);
    }

    /**
     * Parse data before make it available to controller
     */
    function parseData(data) {
      if (data.data.token) {
        $sessionStorage.put('api_token', data.data.token);
      }
      return data.data;
    }
  }
})();
