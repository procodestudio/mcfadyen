describe('Main services', function() {
  beforeEach(module('mcfadyen', 'templates', 'ngSessionStorage'));

  describe('MainModel', function() {
    var MainModel;
    var Constants;
    var $httpBackend;
    var urlProducts;
    var urlShoppingCart;
    var jsonProducts = {
      error: false,
      data: [{id: 1, name: 'Product 1'}, {id: 2, name: 'Product 2'}]
    };
    var jsonShoppingCart = {
      error: false,
      data: {id: 1, amount: 150, items: [{quantity: 1, amount: 150}]}
    };

    beforeEach(inject(function($injector) {
      MainModel = $injector.get('MainModel');
      Constants = $injector.get('Constants');
      urlShoppingCart = Constants.get('API_URL') + '/shoppingcart';
      urlProducts = Constants.get('API_URL') + '/products';

      $httpBackend = $injector.get('$httpBackend');
      $httpBackend.when('GET', urlProducts).respond(200, jsonProducts);
      $httpBackend.when('GET', urlShoppingCart).respond(200, jsonShoppingCart);
    }));

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should exists', function() {
      expect(MainModel).toBeDefined();
    });

    describe('getProducts()', function() {
      it('should exists', function() {
        expect(MainModel.getProducts).toBeDefined();
      });

      it('should return a list of products', function() {
        MainModel
          .getProducts()
          .then(function(data) {
            expect(typeof data).toBe('object');
            expect(data.length).toBeGreaterThan(0);
            expect(data[0].name).toEqual('Product 1');
          });

        $httpBackend.flush();
      });
    });

    describe('getCart()', function() {
      it('should exists', function() {
        expect(MainModel.getCart).toBeDefined();
      });

      it('should return shopping cart items', function() {
        MainModel
          .getCart()
          .then(function(data) {
            expect(typeof data).toBe('object');
            expect(data.id).toEqual(1);
            expect(data.items.length).toBeGreaterThan(0);
          });

        $httpBackend.flush();
      });
    });

    describe('parseData()', function() {
      it('should exists', function() {
        expect(MainModel.parseData).toBeDefined();
      });

      it('should parse object without token server', function() {
        var data = {data: {name: 'mcfadyen'}};
        var response = MainModel.parseData(data);
        expect(response.name).toEqual('mcfadyen');
      });

      it('should parse object with token', function() {
        var data = {data: {token: 'mcfadyen'}};
        var response = MainModel.parseData(data);
        expect(response.token).toEqual('mcfadyen');
      });
    });
  });
});
