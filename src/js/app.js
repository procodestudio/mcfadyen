'use strict';

(function() {
  var modules = [
    'ui.router',
    'ngSessionStorage',
    'mcfadyen.main'
  ];

  angular
    .module('mcfadyen', modules)
    .config(appConfig);

  appConfig.$inject = ['$locationProvider', '$urlRouterProvider', '$qProvider'];

  /**
   * Initial config
   */
  function appConfig($locationProvider, $urlRouterProvider, $qProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $qProvider.errorOnUnhandledRejections(false);
  }
})();
