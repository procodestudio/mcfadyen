var _ = require('lodash');
var express = require('express');
var router = express.Router();
var db = require('./models');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');

/**
 * Get a list of products
 */
router.get('/products', function(req, res) {
  db.Products
    .find()
    .sort([['name', 'ascending']])
    .exec(function(error, data) {
      parseResponse(error, data, res);
    });
});

/**
 * Get shopping cart from current user
 * session or create a new one
 */
router.get('/shoppingcart', function(req, res) {
  var token = req.headers['x-cart-token'];

  if(!token) {
    var shoppingCart = new db.ShoppingCart();
    shoppingCart.items = [];
    shoppingCart.amount = 0;
    shoppingCart.save(function(error, data) {
      data = data.toObject();
      data.token = genToken({cart: data._id});
      parseResponse(error, data, res);
    });
  } else {
    db.ShoppingCart
      .findById(decodeToken(token))
      .exec(function(error, data) {
        parseResponse(error, data, res);
      });
  }
});

/**
 * Route to add or update a product in cart
 */
router.post('/shoppingcart/items', function(req, res) {
  var token = decodeToken(req.headers['x-cart-token']);

  getProduct(req.body.productId)
    .then(function(product) {
      product = product.toObject();
      product.quantity = req.body.quantity;

      getCart(token)
        .then(function(cart) {
          addToCart(cart, product, token, res);
        });
    });
});

/**
 * Route to delete Commerce Item from cart
 */
router.delete('/shoppingcart/items/:id', function(req, res) {
  var token = req.headers['x-cart-token'];

  db.ShoppingCart
    .findById(decodeToken(token))
    .exec(function(err, data) {
      data.items = removeFromCart(data.items, req.params.id);
      data.amount = calcAmount(data.items);
      data.save(function (error, data) {
        parseResponse(error, data, res)
      })
    });
});

/**
 * Decode token to get cart ID
 */
function decodeToken(token) {
  return jwt.verify(token, 'mcfadyen').cart;
}

/**
 * Generate token with cart ID
 */
function genToken(key) {
  return jwt.sign(key, 'mcfadyen', {
    expiresIn: '1 hour'
  })
}

/**
 * Remove Commerce Item from database
 */
function removeFromCart(items, id) {
  return _.reject(items, {id: id});
}

/**
 * Add item to cart object
 */
function addToCart(cart, product, token, res) {
  db.ShoppingCart
    .find({'items.product.name': product.name})
    .where('_id').equals(token)
    .exec(function(err, data) {
      if(data && data.length) {
        cart.items = updateCommerceItems(cart.items, product);
      } else {
        cart.items.push({
          quantity: product.quantity,
          amount: parseFloat(product.price) * parseFloat(product.quantity),
          product: {
            name: product.name,
            price: product.price
          }
        });
      }

      cart.amount = calcAmount(cart.items);
      cart.save(function(err, data) {
        parseResponse(err, data, res);
      })
    });
}

/**
 * Update commerce items from shopping cart
 */
function updateCommerceItems(commerceItems, product) {
  var newCommerceItem = [];

  _.each(commerceItems, function(item) {
    if(item.product.name === product.name) {
      item.quantity = parseInt(item.quantity) + parseInt(product.quantity);
      item.amount = parseFloat(product.price * item.quantity);
      newCommerceItem.push(item);
    } else {
      newCommerceItem.push(item);
    }
  });

  return newCommerceItem;
}

/**
 * Calculate commerce items amount
 */
function calcAmount(items) {
  return _.reduce(items, function(sum, item) {
    return sum + item.amount;
  }, 0);
}

/**
 * Get cart object
 */
function getCart(id) {
  return db.ShoppingCart
    .findById(id)
    .exec();
}

/**
 * Get product object
 */
function getProduct(id) {
  return db.Products
    .findById(id)
    .exec();
}

/**
 * Parse response from actions
 */
function parseResponse(error, data, res) {
  var response;

  if(error) {
    response = {
      error: true,
      message: 'Error: ' + error
    };
  } else {
    response = {
      error: false,
      data: data
    };
  }

  res.write(JSON.stringify(response), function() {
    res.end();
  });
}

module.exports = router;
