var express = require('express');
var cors = require('cors');
var bodyParser= require('body-parser');

var db = require('./db');
var api = require('./api.router');
var port = process.env.PORT || 5000;
var app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static('dist'));
app.use('/api', api);

db()
  .then(connSuccess, connError);

/**
 * Parse success on db connection
 */
function connSuccess() {
  app.listen(port, function() {
    console.log('Node server is running at http://localhost:' + port);
  });
}

/**
 * Parse error on db connection
 */
function connError() {
  console.log('Can\'t connect to database');
}
