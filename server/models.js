'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var commerceItem;
var products;
var product;
var shoppingCart;
var models;

product = new Schema({
  name: {type: String, required: true},
  image: {type: String},
  price: {type: Number, required: true}
});

commerceItem = new Schema({
  quantity: {type: Number, required: true},
  amount: {type: Number, required: true},
  product: product
});

shoppingCart = mongoose.model('shoppingCarts', new Schema({
  items: [commerceItem],
  amount: {type: Number}
}));

products = mongoose.model('products', product);

models = {
  Products: products,
  ShoppingCart: shoppingCart
};

module.exports = models;
