'use strict';

var mongoose = require('mongoose');
var dbOptions;
var db;

dbOptions = {
  useMongoClient: true
};

db = function () {
  return mongoose.connect('mongodb://mcfayden:mcfaydendbuser@ds147882.mlab.com:47882/mcfadyen', dbOptions);
};

module.exports = db;
